<?php

/**
 * Handle callback() response.
 */

if (!array_key_exists('state', $_REQUEST)) {
    throw new \Exception('Lost Client State');
}
$state = \SimpleSAML\Auth\State::loadState(
    $_REQUEST['state'],
    \SimpleSAML\Module\authremoteuser\Auth\Source\RemoteUser::STAGE_INIT
);
assert(array_key_exists(\SimpleSAML\Module\authremoteuser\Auth\Source\RemoteUser::AUTHID, $state));
// find authentication source
$sourceId = $state[\SimpleSAML\Module\authremoteuser\Auth\Source\RemoteUser::AUTHID];
/** @var \SimpleSAML\Module\authremoteuser\Auth\Source\RemoteUser|null $source */
$source = \SimpleSAML\Auth\Source::getById($sourceId);
if ($source === null) {
    throw new \Exception('Could not find authentication source with id '.$sourceId);
}
$source->finalStep($state);
