# remoteuser-simplesamlphp-module

SimpleSAMLphp module for getting user identity from REMOTE_USER variables

See docs/RemoteUser.md for more information.
