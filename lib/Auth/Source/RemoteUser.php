<?php

namespace SimpleSAML\Module\authremoteuser\Auth\Source;

use SimpleSAML\Auth\Source;
use SimpleSAML\Configuration;
use SimpleSAML\Utils\HTTP;
use SimpleSAML\Logger;

/**
 * Getting user's identity from REMOTE_USER.
 *
 * @author Florian Döring, <f.doering@dkfz-heidelberg.de>
 *
 * @package SimpleSAMLphp
 */
class RemoteUser extends \SimpleSAML\Auth\Source
{
    /**
     * The string used to identify our states.
     */
    const STAGE_INIT = 'authremoteuser:init';

    /**
     * The key of the AuthId field in the state.
     */
    const AUTHID = 'authremoteuser:AuthId';

    /**
     * Constructor for this authentication source.
     *
     * @param array $info Information about this authentication source.
     * @param array $config Configuration.
     */
    public function __construct($info, $config)
    {
        assert('is_array($info)');
        assert('is_array($config)');

        Logger::debug('info='.json_encode($info));
        Logger::debug('config='.json_encode($config));

        // Call the parent constructor first, as required by the interface
        parent::__construct($info, $config);

        return;
    }

    /**
     *
     * @param array &$state Information about the current authentication.
     */
    public function authenticate(&$state)
    {
        assert(is_array($state));

        if (!isset($_SERVER['REMOTE_USER'])) {
            $state[self::AUTHID] = $this->authId;
            $stateID = \SimpleSAML\Auth\State::saveState($state, self::STAGE_INIT);
            
            $redirectURL = '/simplesaml/module.php/authremoteuser/protected/callback.php'.
                '?state='.urlencode($stateID);
            HTTP::redirectTrustedURL($redirectURL);
        } else {
            $this->finalStep($state);
        }

        assert(false); // should never be reached
        return;
    }

    /**
     * @param array &$state
     * @return void
     * @throws \Exception
     */
    public function finalStep(&$state)
    {
        $login = null;

        if (isset($_SERVER['REMOTE_USER'])) {
            // $login = preg_replace('/^([^@]*).*/', '\1', $_SERVER['REMOTE_USER']);
            $login = preg_replace('$/.*$', '', $_SERVER['REMOTE_USER']);
        } else {
            // Variable is empty, this shouldn't happen if the web server is properly configured
            Logger::error(
                'authremoteuser: user entered protected area without being properly authenticated'
            );
            $state['authremoteuser.error'] = "AUTHERROR";
            $this->authFailed($state);

            assert(false); // should never be reached
            return;
        }

        Logger::info('authremoteuser: ' . $login);
        $attributes = [];
        $attributes['uid'][] = $login;
        $attributes['email'][] = $_SERVER['OIDC_CLAIM_email'];
        $attributes['name'][] = $_SERVER['OIDC_CLAIM_name'];
        $attributes['entitlement'][] = $_SERVER['OIDC_CLAIM_eduperson_entitlement'];

        assert(is_array($attributes));
        \SimpleSAML\Logger::debug('Returned Attributes: '.implode(", ", array_keys($attributes)));
        
        $state['Attributes'] = $attributes;
        
        $this->authSuccesful($state);

        assert(false); // should never be reached
        return;
    }

    /**
     * Finish a successful authentication.
     *
     * This function can be overloaded by a child authentication class that wish to perform some operations after login.
     *
     * @param array &$state Information about the current authentication.
     */
    public function authSuccesful(&$state)
    {
        Logger::debug('authSuccesful()');

        \SimpleSAML\Auth\Source::completeAuth($state);

        assert(false); // should never be reached
        return;
    }
}
