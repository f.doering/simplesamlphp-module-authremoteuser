# How to use RemoteUser module

The module is just getting result of the authentication done by Apache web server.
Therefore the module doesn't need to cope with any unsuccessful states of login process.
Apache will ensure that users are properly authenticated before they reach this module.
Module then just extract user identifier and additional attributes.

## Apache configuration

## Module configuration

The first thing you need to do is to enable the module:

    touch modules/authremoteuser/enable

Then you must add it as an authentication source. Here is an
example authsources.php entry:

    'remoteuser' => array(
        'authremoteuser:RemoteUser',
        'attributes' => array('cn', 'uid', 'mail', 'ou', 'sn'),
    ),
